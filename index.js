const request = require('request');
const fs = require('fs');

let urls = [
	'http://www.google.com',
	'http://www.facebook.com',
	'http://www.linkedin.com',
	'http://www.instagram.com',
	'http://www.youtube.com',
	'http://www.medium.com',
	'http://www.pinterest.com',
	'http://www.hackernoon.com',
	'http://www.stackoverflow.com',
	'http://www.github.com'
];

// Saves a webpage to a file that is created in ./websites folder.
const saveWebPageToFile = (url) => {
	var urlNoProtocol = url.replace(/^https?\:\/\//i, '');
	var currentPath = process.cwd();
	request(url).pipe(fs.createWriteStream(`${currentPath}/websites/${urlNoProtocol}`));
};

urls.map((url) => {
	saveWebPageToFile(url);
});
